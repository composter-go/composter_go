import 'package:flutter/material.dart';

class presentation extends StatefulWidget {
  @override
  State<presentation> createState() => MaPresentation();
}

class MaPresentation extends State<presentation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Présentation de Composter Go"),
      ),
      body: Center(
        child: Column(
          children: [
            Text(
                "Composter Go est une application innovante qui transforme le compostage en une expérience ludique et éducative. En encourageant les pratiques durables, elle permet aux utilisateurs de réduire leur empreinte écologique tout en découvrant les joies du recyclage organique en milieu urbain."),
            ElevatedButton(
              onPressed: () {
                // L'endroit correct pour utiliser Navigator.push
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                );
              },
              child: Text("Aller sur la page d'Accueil"),
            ),
          ],
        ),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // ... contenu de la classe MyApp ...
        );
  }
}
